var articles = ['106.W21.041', '106.W21.042', '106.W21.043', '110.W21.028', '110.W21.008', '110.W21.007', '113.W21.003', '113.W21.008', '113.W21.028', '119.W21.007', '119.W21.008', '125.W21.041', '125.W21.043', '125.W21.044', '126.W21.008', '126.W21.018', '128.W21.001', '128.W21.007', '129.W21.001', '129.W21.002', '129.W21.007', '135.W21.018', '135.W21.001'];

var sizeToReplace = 'SIZE: 42';
var sizeOptions = ['SIZE: 42', 'SIZE: 44', 'SIZE: 46', 'SIZE: 48', 'SIZE: 50','SIZE: 52', 'SIZE: 54', 'SIZE: 56', 'SIZE: 58'];

var destFolder = Folder.selectDialog( 'Select the folder where you want to save PDF files.', '~' );

var doc = app.activeDocument;
var sizeNumber = 42;

for(var idx = 0; idx < articles.length; idx++) {
   
   sizeNumber = 42;
   
   for (var j = 0; j < 9; j++) { 
        
      doc.textFrames[0].contents = articles[idx];
      doc.textFrames[0].createOutline();
      $.write(j + '\n');
      doc.textFrames[0].contents = sizeOptions[j];
      doc.textFrames[0].createOutline();
      //C:\Users\krovo\Desktop\OUT-script\test
      targetFile = new File(destFolder + '/' + articles[idx] + '/' + articles[idx] + '_size_' + sizeNumber + '.pdf');
      pdfSaveOpts = getPDFOptions();
      doc.saveAs(targetFile, pdfSaveOpts);
      undo();

      sizeNumber++;
      sizeNumber++;
      $.write(sizeNumber + '\n');
   }
}

activeDocument.close(SaveOptions.DONOTSAVECHANGES);

function getPDFOptions() {
   var pdfSaveOpts = new PDFSaveOptions();
   pdfSaveOpts.acrobatLayers = false;
   pdfSaveOpts.colorBars = false;
   pdfSaveOpts.colorCompression = CompressionQuality.AUTOMATICJPEGHIGH;
   pdfSaveOpts.compressArt = true;
   pdfSaveOpts.embedICCProfile = true;
   pdfSaveOpts.enablePlainText = true;
   pdfSaveOpts.generateThumbnails = false;
   pdfSaveOpts.optimization = true;
   pdfSaveOpts.pageInformation = false;
   return pdfSaveOpts;
}
