var articles = ['102.W21.051', '102.W21.057', '102.W21.023', '104.W21.051', '104.W21.057', '104.W21.023', '115.W21.010', '115.W21.023'];

var sizeToReplace = 'SIZE: 42';
var sizeOptions = ['SIZE: 42', 'SIZE: 44', 'SIZE: 46', 'SIZE: 48', 'SIZE: 50','SIZE: 52', 'SIZE: 54', 'SIZE: 56', 'SIZE: 58'];

var destFolder = Folder.selectDialog( 'Select the folder where you want to save PDF files.', '~' );

var doc = app.activeDocument;
var sizeNumber = 42;

for(var idx = 0; idx < articles.length; idx++) {
   
   sizeNumber = 42;
   
   for (var j = 0; j < 9; j++) { 
        
      doc.textFrames[0].contents = articles[idx];
      doc.textFrames[0].createOutline();
      $.write(j + '\n');
      doc.textFrames[0].contents = sizeOptions[j];
      doc.textFrames[0].createOutline();
      //C:\Users\krovo\Desktop\OUT-script\test
      targetFile = new File(destFolder + '/' + articles[idx] + '/' + articles[idx] + '_size_' + sizeNumber + '.pdf');
      pdfSaveOpts = getPDFOptions();
      doc.saveAs(targetFile, pdfSaveOpts);
      undo();

      sizeNumber++;
      sizeNumber++;
      $.write(sizeNumber + '\n');
   }
}

activeDocument.close(SaveOptions.DONOTSAVECHANGES);

function getPDFOptions() {
   var pdfSaveOpts = new PDFSaveOptions();
   pdfSaveOpts.acrobatLayers = false;
   pdfSaveOpts.colorBars = false;
   pdfSaveOpts.colorCompression = CompressionQuality.AUTOMATICJPEGHIGH;
   pdfSaveOpts.compressArt = true;
   pdfSaveOpts.embedICCProfile = true;
   pdfSaveOpts.enablePlainText = true;
   pdfSaveOpts.generateThumbnails = false;
   pdfSaveOpts.optimization = true;
   pdfSaveOpts.pageInformation = false;
   return pdfSaveOpts;
}
